----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08/20/2018 03:42:24 AM
-- Design Name: 
-- Module Name: exp_2_1 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity exp_2_1 is
    Port ( x1 : in STD_LOGIC;
           x2 : in STD_LOGIC;
           x3 : in STD_LOGIC;
           x4 : in STD_LOGIC;
           x5 : in STD_LOGIC;
           x6 : in STD_LOGIC;
           x7 : in STD_LOGIC;
           x8 : in STD_LOGIC;
           x9 : in STD_LOGIC;
           x10 : in STD_LOGIC;
           A : out STD_LOGIC);
end exp_2_1;

architecture Behavioral of exp_2_1 is

component or_2_in is
    Port ( A : in STD_LOGIC;
           B : in STD_LOGIC;
           C : out STD_LOGIC);
end component;

signal i1,i2,i3,i4,i5,i6,i7,i8 :std_logic;

begin

h1 : or_2_in port map(x1,x2,i1);
h2 : or_2_in port map(i1,x3,i2);
h3 : or_2_in port map(i2,x4,i3);
h4 : or_2_in port map(i3,x5,i4);
h5 : or_2_in port map(i4,x6,i5);
h6 : or_2_in port map(i5,x7,i6);
h7 : or_2_in port map(i6,x8,i7);
h8 : or_2_in port map(i7,x9,i8);
h9 : or_2_in port map(i8,x10,A);
