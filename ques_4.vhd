----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08/20/2018 07:56:15 AM
-- Design Name: 
-- Module Name: eight_stage_network - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity eight_stage_network is
    Port ( x1 : in STD_LOGIC;
           x2 : in STD_LOGIC;
           x3 : in STD_LOGIC;
           x4 : in STD_LOGIC;
           x5 : in STD_LOGIC;
           x6 : in STD_LOGIC;
           x7 : in STD_LOGIC;
           x8 : in STD_LOGIC;
           x9 : in STD_LOGIC;
           x10 : in STD_LOGIC;
           X : out STD_LOGIC);
end eight_stage_network;

architecture Behavioral of eight_stage_network is
component or_3_in is
    Port ( A : in STD_LOGIC;
           B : in STD_LOGIC;
           C : in STD_LOGIC;
           X : out STD_LOGIC);
end component;

component or_2_in is
    Port ( A : in STD_LOGIC;
           B : in STD_LOGIC;
           C : out STD_LOGIC);
end component;

signal s1,s2,s3,s4,s5 : std_logic;

begin
h1 : or_3_in port map(x1,x2,x3,s1);
h2 : or_3_in port map(x4,x5,x6,s2);
h3 : or_3_in port map(x7,x8,x9,s3);
h4 : or_2_in port map(s1,s2,s4);
h5 : or_2_in port map(s3,s4,s5);
h6 : or_2_in port map(s5,x10,X);

end Behavioral;